/*Leveling up Charik

Increase HP 1d20+1
Increase spell slots (see table)
Add extra feature
Learn 2 spells
*/

console.log('Charik');
const level = 8;

console.log('Wizard level', level);

// Prof bonus: 7 plus your level, divided by 4, rounded down.

const prof = Math.floor((7 + level)/4);

console.log('Proficiency bonus', prof);

// Spell save DC = 8 + your proficiency bonus + your Intelligence modifier


const intelMod = 4;
const ssdc = 8 + prof + intelMod;
console.log('Spell save DC', ssdc);


// Spell attack modifier = your proficiency bonus + your Intelligence modifier
// Attack = d20+attack modifier against opponent's AC
const samod = prof + intelMod;
console.log('Spell attack mod', samod);

console.log('Nr of prepared spells', level + intelMod);
